import os
import requests
from bs4 import BeautifulSoup
NAME="z80"
os.chdir(NAME)

url = "http://0x04.net/~mwk/doc/"+NAME+"/"
html_doc = requests.get(url)
soup = BeautifulSoup(html_doc.text, 'html.parser')

for link in soup.find_all("a"):
    file_name = link.get('href')
    if file_name == "../":
        continue
    elif "pdf" in file_name: 
        data = requests.get(str(url) + file_name)
        print(file_name)
        with open(file_name, 'wb') as f:
            f.write(data.content)
    elif "PDF" in file_name:
        data = requests.get(str(url) + file_name)
        print(file_name)
        with open(file_name, 'wb') as f:
            f.write(data.content)
    else:
        os.mkdir(file_name)
        print("!!! new dir " + file_name)
    