import os
import requests
from bs4 import BeautifulSoup

work = 1
page = 147
os.chdir("MP3")
while work == 1:
    print(page)
    url = "https://freemusicarchive.org/genre/Instrumental?sort=track_date_published&d=1&page=" + str(page)
    html_doc = requests.get(url)
    soup = BeautifulSoup(html_doc.text, 'html.parser')
    block = soup.find_all("a", "icn-arrow")

    if not block:
        work = 0
        print("fertig")
    else:
        for link in block:
            file_name = link.get('href').split('/')[-1]
            mp3 = requests.get(link.get('href'))
            with open(file_name, 'wb') as f:
                f.write(mp3.content)
        page += 1